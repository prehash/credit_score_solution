require 'spec_helper'
require 'loans'

describe Loans do
	let(:path) { 'data/loans.txt' }
	
	it 'should load list of companies form a given file' do
		loans = Loans.load(path)
		
		loans.should be_instance_of Loans
		loans.length.should eq 1000
		
		first = loans.first
		
		first.should be_instance_of Loan
		
		first.lender_id.should eq '03302845'
		first.borrower_id.should eq '02895128'
		first.amount.should eq 5_795_000
		first.loan_date.should eq Date.parse('2014-01-12')
		first.due_date.should eq Date.parse('2014-01-12')
		first.payment_date.should eq Date.parse('2014-01-12')
	end
	
	describe 'from_company' do
		it 'should return filtered list of loans' do
			loans = Loans.load(path)
			loans = loans.from_company('03302845')
			
			loans.map(&:lender_id).uniq.first.should eq '03302845'
		end
	end
	
	describe 'to_company' do
		it 'should return filtered list of loans' do
			loans = Loans.load(path)
			loans = loans.to_company('03302845')

			loans.map(&:borrower_id).uniq.first.should eq '03302845'
		end
	end
	
	it do
		loans = Loans.load(path)
		puts loans.to_company('02894435').select(&:overdue?).map {|l| l.lender_id }
	end
end