require 'spec_helper'
require 'loan'

describe Loan do
	describe '#paid' do 
		let(:loan) { Loan.new('1', '2', 100, Date.today, Date.today, nil) }
		
		subject { loan.paid? }
		
		context 'not paid yet' do
			before { loan.payment_date = nil }

			it { should eq false }
		end
		
		context 'paid' do
			it { should eq false }
		end
	end
	
	describe '#overdue?' do
		let(:loan) { Loan.new('1', '2', 100, Date.parse('1st Jan 2014'), Date.parse('10st Jan 2014'), nil) }
		
		subject { loan.overdue? }
		
		context 'not paid yet' do
			before { loan.payment_date = nil }
			
			it { should eq true }
		end
		
		context 'paid late' do
			before { loan.payment_date = Date.parse('10st Jan 2014') + 32 }
			
			it { should eq true }
		end
		
		context 'paid on time' do
			before { loan.payment_date = Date.parse('10st Jan 2014') }
			
			it { should eq false }
		end
		
		context 'paid 31 days late' do
			before { loan.payment_date = Date.parse('10st Jan 2014') + 31 }
			
			it { should eq false }
		end
		
		context 'not paid and due' do
			before { 
				loan.due_date = Date.today + 10
			}
			
			it { should eq true }
		end
		
		context 'not paid and due' do
			before { 
				loan.due_date = Date.today + 31
			}
			
			it { should eq false }
		end
	end
end