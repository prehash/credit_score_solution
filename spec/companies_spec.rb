require 'spec_helper'
require 'companies'

describe Companies do
	let(:path) { 'data/companies.txt' }
	
	it 'should load list of companies form a given file' do
		companies = Companies.load(path)
		
		companies.should be_instance_of Array
		companies.length.should eq 30
		
		first = companies.first
		
		first.should be_instance_of Company
		
		first.name.should eq 'Excellpak Limited'
		first.id.should eq '00007941'
	end
end