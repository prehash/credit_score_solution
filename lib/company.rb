class Company < Struct.new(:name, :id)
	def balance(loans)
		loans.to_company(id).total_not_paid - loans.from_company(id).total_not_paid
	end
end