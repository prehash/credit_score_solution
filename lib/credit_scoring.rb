require 'date'
require 'loans'
require 'companies'

class CreditScoring
  def initialize(companies_path, loans_path)
  	@loans = Loans.load('data/loans.txt')
  	@companies = Companies.load('data/companies.txt')
  end

  def clear_companies
    @companies.reject {|company| @loans.to_company(company.id).any?(&:overdue?) }
  end

  def companies_with_balance(min)
    @companies.select {|company| company.balance(@loans) > min }
  end
end