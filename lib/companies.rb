require 'company'
require 'file_loader'

class Companies
	extend FileLoader
	
	def self.load(path)
		companies = []
		
		load_file_lines(path) do |line|
  		tokens = line.split(' ')

	    id = tokens.pop
			companies << Company.new(tokens.join(' '), id)
		end
		
		companies
	end
end