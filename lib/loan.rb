class Loan < Struct.new(:lender_id, :borrower_id, :amount, :loan_date, :due_date, :payment_date)
	def paid?
		!!payment_date
	end
	
	def not_paid?
		!payment_date
	end
	
	def overdue?
		due? ? due_date < (Date.today + 31) : !paid_on_time?
	end
	
	private
	
	def paid_on_time?
		paid? && payment_date <= due_date + 31
	end

	def due?
		due_date > Date.today
	end
end