module FileLoader
	def load_file_lines(path)
		File.open(path, 'r') do |file|
		  while (line = file.gets)
    		yield(line)
  		end
		end
	end
end