require 'company'
require 'date'
require 'loan'
require 'file_loader'

class Loans < Array
	extend FileLoader
	
	def from_company(id)
		Loans.new(select {|loan| loan.lender_id === id})
	end
	
	def to_company(id)
		Loans.new(select {|loan| loan.borrower_id === id})
	end
	
	def total_not_paid
		select(&:not_paid?).map(&:amount).reduce(0, :+)
	end
	
	def self.load(path)
		loans = Loans.new
		
		load_file_lines(path) do |line|
  		lender_id, borrower_id, amount, loan_date, due_date, payment_date = line.split(' ')
  		
  		amount = amount.to_i
  		loan_date = Date.parse(loan_date)
  		due_date = Date.parse(due_date)
  		payment_date = payment_date === 'nil' ? nil : Date.parse(payment_date)
  		
  		loans << Loan.new(lender_id, borrower_id, amount, loan_date, due_date, payment_date)
		end
		
		loans
	end
end